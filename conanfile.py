from conans import ConanFile, CMake

class CxxProfConan(ConanFile):
    name = "cxxprof_dyn_network"
    version = "1.1.0"
    url = "https://gitlab.com/groups/CxxProf"
    license = "GNU LESSER GENERAL PUBLIC LICENSE Version 3"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    default_options = "ZMQ:shared=True"
    exports = "*"

    def requirements(self):
        self.requires.add("Pluma/1.1@monsdar/testing")
        self.requires.add("Boost/1.59.0@lasote/stable")
        self.requires.add("ZMQ/4.1.1@memsharded/stable")
        self.requires.add("zmqcpp/4.1.1@memsharded/testing")
        self.requires.add("cxxprof_preloader/1.1.1@monsdar/testing")
        
    def build(self):
        cmake = CMake(self.settings)
        self.run('cmake %s %s' % (self.conanfile_directory, cmake.command_line))
        self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.h", dst="include/cxxprof_dyn_network", src="%s/cxxprof_dyn_network" % self.conanfile_directory)
        self.copy("*.dll", dst="bin", src="bin")
        self.copy("*.so", dst="bin", src="bin")
        self.copy("*.lib", dst="lib", src="lib")
        self.copy("*.a", dst="lib", src="lib")

    def package_info(self):
        self.cpp_info.libs = ["cxxprof_dyn_network"]
