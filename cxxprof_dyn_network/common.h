
#pragma once

#ifdef WIN32
	#ifdef CXXPROF_DYN_NETWORK
	    #define CxxProf_Dyn_Network_EXPORT __declspec( dllexport )
	#else
	    #define CxxProf_Dyn_Network_EXPORT __declspec( dllimport )
	#endif //CXXPROF_DYN_NETWORK
#else
	#define CxxProf_Dyn_Network_EXPORT 
#endif
