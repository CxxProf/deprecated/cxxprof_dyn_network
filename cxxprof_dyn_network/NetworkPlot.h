
#pragma once

#include "cxxprof_dyn_network/common.h"
#include <boost/cstdint.hpp>
#include <stdint.h>
#include <string>

namespace CxxProf
{

    struct CxxProf_Dyn_Network_EXPORT NetworkPlot
    {
        std::string Name;
        boost::int64_t Timestamp;
        double Value;

        NetworkPlot() :
            Name(""),
            Timestamp(0),
            Value(0.0)
        {}
    };

}
